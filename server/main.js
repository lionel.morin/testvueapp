import { Meteor } from 'meteor/meteor';
import Links from '/imports/api/links';

function insertLink(title, description, url) {
  Links.insert({ title, description, url, createdAt: new Date() });
}

Meteor.startup(() => {
  // If the Links collection is empty, add some data.
  if (Links.find().count() === 0) {
    insertLink(
      "Garrosh Hellscream",
      "Bonjour madame.",
      "http://wow.zamimg.com/images/hearthstone/cards/enus/medium/HERO_01.png"
    );

    insertLink(
      "Mind Control",
      "Take control of an enemy minion.",
      "http://wow.zamimg.com/images/hearthstone/cards/enus/medium/CS1_113.png"
    );

    insertLink(
      "Prophet Velen",
      "Double the damage and healing of your spells and Hero Power.",
      "http://wow.zamimg.com/images/hearthstone/cards/enus/medium/EX1_350.png"
    );

    insertLink(
      "Mana Addict",
      "Whenever you cast a spell, gain +2 Attack this turn.",
      "http://wow.zamimg.com/images/hearthstone/cards/enus/medium/EX1_055.png"
    );
  }
});