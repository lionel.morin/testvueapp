import { Mongo } from 'meteor/mongo';
import { Meteor } from "meteor/meteor";
import { check } from 'meteor/check';
import { Match } from 'meteor/check'

export default Links = new Mongo.Collection('links');

const NonEmptyString = Match.Where((x) => {
    check(x, String);
    return x.length > 0;
});

Meteor.methods({
    'links.insert'(title, description, url) {
      check(title, NonEmptyString);
      check(description, NonEmptyString);
      check(url, NonEmptyString);
      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized');
      }
 
      Links.insert({
        title,
        description,
        url
      });
    },
});